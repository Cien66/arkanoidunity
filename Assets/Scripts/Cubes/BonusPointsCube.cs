﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusPointsCube : BasicCubeS {
#region begin Serialized
    [Range(1,10)]
    [Tooltip("Multiplier basic points for bonus")]
    [SerializeField] int pointsMultiplier = 2;
#endregion

    // Use this for initialization
    void Start () {
        points *= pointsMultiplier;
	}
}
