﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowMotionCube : BasicCubeS
{
    #region begin Serialized
    [Tooltip("New speed for ball")]
    [SerializeField]
    float slowerSpeedBall = 2.0f;

    [Tooltip("New speed for palette")]
    [SerializeField]
    float slowerSpeedPalette = 2.0f;

    [SerializeField] float effectTime = 3.0f;
    #endregion

    #region begin Property
    public float SpeedBall { get { return slowerSpeedBall; } }
    public float SpeedPalette { get { return slowerSpeedPalette; } }
    public float EffectTime { get { return effectTime; } }
    #endregion
}
