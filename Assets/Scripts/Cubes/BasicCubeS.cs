﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class CubeSave
{
    public float x;
    public float y;

    public BasicCubeS.TypeCube type;
}

public class BasicCubeS : MonoBehaviour
{
    #region begin Public
    public enum TypeCube { Basic, Bonus, SlowMotion, Health };
    #endregion

    #region begin Protected 
    [SerializeField] protected int points = 10;

    [SerializeField] protected TypeCube type;
    #endregion

    public int GetScore()
    {
        return points;
    }

    public TypeCube GetCubeType()
    {
        return type;
    }
}
