﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]

public class BallTrigger : MonoBehaviour {

    void HandleCube(GameObject cube) {
        BasicCubeS.TypeCube type = cube.GetComponentInParent<BasicCubeS>().GetCubeType();

        switch (type) {
            case BasicCubeS.TypeCube.Basic:
            case BasicCubeS.TypeCube.Bonus:
            LevelController.Instance.AddScore(cube.GetComponentInParent<BasicCubeS>().GetScore());
            break;
            case BasicCubeS.TypeCube.SlowMotion:
            var slowMotion = cube.GetComponentInParent<SlowMotionCube>();
            LevelController.Instance.RunSlowMotion(slowMotion.SpeedBall, slowMotion.SpeedPalette, slowMotion.EffectTime);
            LevelController.Instance.AddScore(cube.GetComponentInParent<BasicCubeS>().GetScore());
            break;
            case BasicCubeS.TypeCube.Health:
            var health = cube.GetComponentInParent<HealthCube>();
            LevelController.Instance.AddHealth(health.GetHealt());
            LevelController.Instance.AddScore(cube.GetComponentInParent<BasicCubeS>().GetScore());
            break;
        }
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag == "Cube") {
            HandleCube(collision.gameObject);
            LevelController.Instance.CheckWin();
            Destroy(collision.gameObject.transform.parent.gameObject);
        }
    }
}
