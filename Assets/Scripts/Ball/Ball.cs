﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BallSave
{
    public float pX;
    public float pY;

    public float dX;
    public float dY;

    public float speed;
}

[RequireComponent(typeof(SphereCollider))]
[RequireComponent(typeof(Rigidbody))]

public class Ball : MonoBehaviour
{
    #region begin Private
    Rigidbody rigidbody;
    #endregion

    #region begin Serialized
    [SerializeField] float speed = 3.0f;
    #endregion

    #region begin Property
    public float Speed
    {
        get { return speed; }
        set
        {
            speed = value;

            rigidbody.velocity = rigidbody.velocity.normalized * speed;
        }
    }

    public Vector3 Direction
    {
        get { return rigidbody.velocity.normalized; }
        set { rigidbody.velocity = value.normalized * speed; }
    }
    #endregion
    // Use this for initialization
    void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Vector3 dir = rigidbody.velocity.normalized;

        if (rigidbody.velocity.x < .2f && rigidbody.velocity.x > -.2f)
        {
            rigidbody.velocity = new Vector3(.3f, dir.y, 0) * speed;
        }
        if (rigidbody.velocity.y < .2f && rigidbody.velocity.y > -.2f)
        {
            rigidbody.velocity = new Vector3(dir.x, 0.3f, 0) * speed;
        }
    }

    public void RandDirection()
    {
        float x;
        float y;

        do
        {
            x = UnityEngine.Random.Range(-1.0f, 1.0f);
        } while (x > 0.3f || x < -0.3f);

        y = UnityEngine.Random.Range(0.15f, 1.0f);

        Vector3 direction = new Vector3(x, y, 0).normalized;

        rigidbody.velocity = direction * speed;
    }
}
