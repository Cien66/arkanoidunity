﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    private void Start()
    {
        Cursor.visible = true;
    }

    public void ContinueGame()
    {
        Cursor.visible = false;
        gameObject.active = false;
        GameController.Instance.ContinueGame();
        if (LevelController.Instance)
            LevelController.Instance.Pause = false;
    }

    public void CreatePlayer()
    {
        Application.LoadLevel(3);
    }

    public void NewGame()
    {
        GameController.Instance.NewGame();
    }

    public void MainMenu()
    {
        if (Application.loadedLevel == 1)
            LevelController.Instance.SaveGame();

        Application.LoadLevel(0);
    }

    public void TopBoard()
    {
        GameController.Instance.TableResults();
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void ClearTopBoard()
    {
        GameController.Instance.ClearTopTable();
    }
    
}
