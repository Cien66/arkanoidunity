﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCubeSystem : MonoBehaviour
{
    #region begin Protected
    protected int cubes = 0;

    int helthCubes;
    int slowMotionCube;
    int bonusCubes;
    #endregion

    #region begin Serialized


    [Tooltip("Left up and right down corners board")]
    [SerializeField]
    Transform[] cornerBoard = new Transform[2];

    [Header("Cubes")]
    [SerializeField]
    protected List<GameObject> cubesPrefabs;

    [Tooltip("Min cubes on maps")]
    [SerializeField]
    protected int minCubes = 10;

    [Tooltip("Max cubes on maps")]
    [SerializeField]
    protected int maxCubes = 50;

    [Range(0, .3f)]
    [Tooltip("Bonus cubes on round per all cubes (%)")]
    [SerializeField]
    protected float bonusCubePerc;

    [Range(0, 0.3f)]
    [Tooltip("Health cubes on round per all cubes (%)")]
    [SerializeField]
    protected float healthCubePerc;

    [Range(0, .3f)]
    [Tooltip("Slow motion cubes on round per all cubes (%)")]
    [SerializeField]
    protected float slowMotionPerc;
    #endregion

    public void MixCubes()
    {
        cubes = Random.Range(minCubes, maxCubes);

        bonusCubes = (int)(cubes * bonusCubePerc);
        helthCubes = (int)(cubes * healthCubePerc);
        slowMotionCube = (int)(cubes * slowMotionPerc);
    }

    public void MixCubes(int nimCubes, int maxCubes)
    {
        this.minCubes = nimCubes;
        this.maxCubes = maxCubes;

        cubes = Random.Range(minCubes, maxCubes);

        bonusCubes = (int)(cubes * bonusCubePerc);
        helthCubes = (int)(cubes * healthCubePerc);
        slowMotionCube = (int)(cubes * slowMotionPerc);

    }

    public void SpawnCubes()
    {
        int i = 0;
        Vector3 spawnPosition;

        for (int j = 0; j < bonusCubes; ++j, ++i)
        {

            spawnPosition = RandPosition();
            Instantiate(cubesPrefabs[1], spawnPosition, Quaternion.identity);
        }

        for (int j = 0; j < helthCubes; ++j, ++i)
        {
            spawnPosition = RandPosition();
            Instantiate(cubesPrefabs[3], spawnPosition, Quaternion.identity);
        }

        for (int j = 0; j < slowMotionCube; ++j, ++i)
        {
            spawnPosition = RandPosition();
            Instantiate(cubesPrefabs[2], spawnPosition, Quaternion.identity);
        }

        for (; i < cubes; ++i)
        {
            spawnPosition = RandPosition();
            Instantiate(cubesPrefabs[0], spawnPosition, Quaternion.identity);
        }
    }

    public void SpawnCubes(BasicCubeS.TypeCube type, Vector3 spawnPosition)
    {
        switch (type)
        {
            case BasicCubeS.TypeCube.Basic:
                Instantiate(cubesPrefabs[0], spawnPosition, Quaternion.identity);
                break;
            case BasicCubeS.TypeCube.Bonus:
                Instantiate(cubesPrefabs[1], spawnPosition, Quaternion.identity);
                break;
            case BasicCubeS.TypeCube.SlowMotion:
                Instantiate(cubesPrefabs[2], spawnPosition, Quaternion.identity);
                break;
            case BasicCubeS.TypeCube.Health:
                Instantiate(cubesPrefabs[3], spawnPosition, Quaternion.identity);
                break;
        }
    }

    Vector3 RandPosition()
    {
        float x;
        float y;

        do
        {
            x = Random.Range(cornerBoard[0].position.x, cornerBoard[1].position.x);
            y = Random.Range(cornerBoard[0].position.y, cornerBoard[1].position.y);
        } while(Physics.CheckSphere(new Vector3(x, y, 0), 2.25f));

        return new Vector3(x, y, 0);
    }
}
