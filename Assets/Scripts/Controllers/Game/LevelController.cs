﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[Serializable]
public class ResultSave
{
    public int Score { get; set; }
    public string PlayerName { get; set; }
}

[Serializable]
public class LevelControllerSave
{
    public int level;
    public int score;
    public int health;

    public float levelSpeedBall;
    public float currentSpeedBall;

    public float currentSpeedPalette;
    public float levelSpeedPalette;

    public float timeSlowMotion;
    public bool isSlowMotion;

    public bool isGameOver;

    public string currentPlayer;
}

public class LevelController : SpawnCubeSystem
{
    #region begin Private
    static LevelController instance = null;

    GameObject ball;
    GameObject palette;

    int score = 0;
    int level = 1;

    float levelSpeedBall;
    float currentSpeedBall;
    float levelSpeedPalette;
    float currentSpeedPalette;
    float timeSlowMotion = 0.0f;

    bool isSlowMotion = false;
    bool isPause = false;
    bool isGameOver = false;
    bool skipEscape = false;
    bool isLoadedGame = false;

    string playerName = "Player";
    #endregion

    #region begin Serialized
    [Space]
    [Tooltip("Transform to spawn ball for next live game")]
    [SerializeField]
    Transform spawnPoint;

    [SerializeField] GameObject ballPrefab;

    [SerializeField] GameObject palettePrefab;

    [Header("GUI")]
    [SerializeField]
    Text scoreText;

    [SerializeField] Text healthText;

    [SerializeField] Text gameOverText;

    [SerializeField] Canvas menuPause;

    [Space]
    [SerializeField]
    int health = 3;
    #endregion

    #region begin Property
    // Game Instance Singleton
    public static LevelController Instance { get { return instance; } }

    public bool SlowMotion { get { return isSlowMotion; } set { isSlowMotion = value; } }

    public bool Pause { get { return isPause; } set { isPause = value; } }

    public int Health { get { return health; } }

    public int Level { get { return level; } }

    public int Score { get { return score; } }

    public float LevelSpeedBall { get { return levelSpeedBall; } }

    public float LevelSpeedPalette { get { return levelSpeedPalette; } }

    public float CurrentSpeedBall { get { return currentSpeedBall; } }

    public float CurrentSpeedPalette { get { return currentSpeedPalette; } }

    public float TimeSlowMotion { get { return timeSlowMotion; } }

    public bool GameOver { get { return isGameOver; } }

    public string PlayerName { get { return playerName; } }
    #endregion


    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        instance = this;

        ball = (GameObject)Instantiate(ballPrefab, spawnPoint.position, Quaternion.identity);
        palette = (GameObject)Instantiate(palettePrefab, new Vector3(spawnPoint.position.x, -4.0f, 0), Quaternion.identity);
    }

    private void OnLevelWasLoaded(int level)
    {
        Debug.Log("Level LOADED: " + level);

        if (GameController.Instance.Loaded && !GameController.Instance.SaveObject.gameManager.isGameOver)
        {
            LoadGame(GameController.Instance.SaveObject);
        }
        else
        {
            NewGame();
        }
    }

    public void NewGame()
    {

        MixCubes();
        SpawnCubes();

        ball.GetComponent<Ball>().RandDirection();
        
        if (!isLoadedGame)
        {
            levelSpeedBall = ball.GetComponent<Ball>().Speed;
            currentSpeedBall = levelSpeedBall;

            levelSpeedPalette = palette.GetComponent<PlayerController>().Speed;
            currentSpeedPalette = levelSpeedPalette;
        }

        gameOverText.gameObject.active = false;
        isPause = false;

        SetHealthText();

        Cursor.visible = false;
        
        playerName = GameController.Instance.PlayerName;

        Time.timeScale = 1.0f;

    }

    public void LoadGame(GlobalSaveObject save)
    {
        score = save.gameManager.score;
        level = save.gameManager.level;
        health = save.gameManager.health;

        currentSpeedBall = save.gameManager.currentSpeedBall;
        levelSpeedBall = save.gameManager.levelSpeedBall;

        currentSpeedPalette = save.gameManager.currentSpeedPalette;
        levelSpeedPalette = save.gameManager.levelSpeedPalette;
        isSlowMotion = save.gameManager.isSlowMotion;
        timeSlowMotion = save.gameManager.timeSlowMotion;

        playerName = save.gameManager.currentPlayer;

        ball.transform.position = new Vector3(save.ball.pX, save.ball.pY, 0);
        ball.GetComponent<Ball>().Speed = save.ball.speed;
        ball.GetComponent<Ball>().Direction = new Vector3(save.ball.dX, save.ball.dY, 0);

        foreach (CubeSave cube in save.cubes)
        {
            Vector3 pos = new Vector3(cube.x, cube.y, 0);
            SpawnCubes(cube.type, pos);
        }

        scoreText.text = "Score:  " + score;
        healthText.text = "Health: " + health;
    }

    private void Update()
    {
        if (isSlowMotion)
        {
            timeSlowMotion -= Time.deltaTime;

            if (timeSlowMotion <= 0.0f)
                StopSlowMotion();
        }

        if (isPause)
        {
            Time.timeScale = 0.0f;
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                OffPause();
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape) && !skipEscape)
        {
            OpenPauseMenu();
        }

        if (isGameOver)
        {
            if (Input.anyKeyDown)
            {
                menuPause.GetComponent<MenuController>().MainMenu();
            }
        }

        skipEscape = false;
    }

    void OffPause()
    {
        Time.timeScale = 1.0f;
        menuPause.gameObject.active = false;
        skipEscape = true;
        isPause = false;
    }

    public void LostRound()
    {
        --health;
        SetHealthText();

        if (health == 0)
        {
            LostGame();
        }

        ball.transform.position = spawnPoint.position;
        ball.GetComponent<Ball>().RandDirection();
    }

    void LostGame()
    {
        SaveGame();


        if (SaveGameSystem.DoesSaveGameExist("mySave.sav"))
            SaveGameSystem.DeleteSaveGame("mySave.sav");


        gameOverText.text += "\nScore: " + score;
        gameOverText.gameObject.active = true;

        Time.timeScale = 0.0f;
        isGameOver = true;
        
        GameController.Instance.AddNewResutl(playerName, score);
    }

    public void AddScore(int score)
    {
        this.score += score;

        scoreText.text = "Score:  " + this.score;
    }


    public void AddHealth(int health)
    {
        this.health += health;
        SetHealthText();
    }

    public void SetHealthText()
    {
        healthText.text = "Health: " + health;
    }

    public void OpenPauseMenu()
    {
        isPause = true;
        menuPause.gameObject.active = true;

        Cursor.visible = true;
    }

    public void RunSlowMotion(float ball, float palette, float time)
    {
        currentSpeedBall = ball;
        currentSpeedPalette = palette;
        timeSlowMotion = time;

        UpdateSpeeds();

        isSlowMotion = true;
    }

    public void StopSlowMotion()
    {
        currentSpeedPalette = levelSpeedPalette;
        currentSpeedBall = levelSpeedBall;

        UpdateSpeeds();

        isSlowMotion = false;
    }

    void UpdateSpeeds()
    {
        ball.GetComponent<Ball>().Speed = currentSpeedBall;
        palette.GetComponent<PlayerController>().Speed = currentSpeedPalette;
    }

    public void CheckWin()
    {
        --cubes;

        if (cubes == 0)
        {
            ++level;

            levelSpeedBall += level;
            levelSpeedPalette += (level / 2);
            UpdateSpeeds();

            MixCubes(minCubes + 5, maxCubes + 5);
            SpawnCubes();
        }
    }

    public void SaveGame()
    {
        GlobalSaveObject saveObj = new GlobalSaveObject();

        saveObj.gameManager = SerializableGameplay.SerializelevelController(this);
        saveObj.ball = SerializableGameplay.SerializeBall(ball.GetComponent<Ball>());

        var cubes = GameObject.FindGameObjectsWithTag("Cube");

        foreach (var cube in cubes)
        {
            BasicCubeS s = cube.GetComponent<BasicCubeS>();
            if (s)
            {
                var c = SerializableGameplay.SerializeCube(s);
                saveObj.cubes.Add(c);
            }
        }

        SaveGameSystem saveSystem = new SaveGameSystem();

        saveSystem.SaveGame(saveObj, "mySave.sav");
    }
}