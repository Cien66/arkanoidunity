﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveGameSystem
{

    private static string GetSavePath(string name)
    {
        return Path.Combine(Application.dataPath, name);
    }

    public bool SaveGame(GlobalSaveObject saveObject, string name)
    {
        BinaryFormatter formatter = new BinaryFormatter();

        using (FileStream stream = new FileStream(GetSavePath(name), FileMode.OpenOrCreate))
        {

            try
            {
                formatter.Serialize(stream, saveObject);
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
                stream.Close();
                return false;
            }
        }

        return true;
    }

    public static GlobalSaveObject LoadGame(string name)
    {
        if (!DoesSaveGameExist(name))
        {
            return null;
        }

        BinaryFormatter formatter = new BinaryFormatter();

        using (FileStream stream = new FileStream(GetSavePath(name), FileMode.Open))
        {

            try
            {
                return formatter.Deserialize(stream) as GlobalSaveObject;
            }
            catch (Exception)
            {
                stream.Close();
                return null;
            }
        }
    }

    public static bool SaveTopTable(List<ResultSave> saveObject, string name)
    {
        BinaryFormatter formatter = new BinaryFormatter();

        using (FileStream stream = new FileStream(GetSavePath(name), FileMode.OpenOrCreate))
        {

            try
            {
                formatter.Serialize(stream, saveObject);
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
              
                return false;  
            }
        }

        return true;
    }

    public static List<ResultSave> LoadTopTable(string name)
    {
        if (!DoesSaveGameExist(name))
        {
            return null;
        }

        BinaryFormatter formatter = new BinaryFormatter();

        using (FileStream stream = new FileStream(GetSavePath(name), FileMode.Open))
        {

            try
            {
                return formatter.Deserialize(stream) as List<ResultSave>;
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);

                return null;
            }
        }
    }

    public static bool DeleteSaveGame(string name)
    {
        try
        {
            string path = GetSavePath(name);
            
            File.Delete(GetSavePath(path));
        }
        catch (Exception)
        {
            return false;
        }

        return true;
    }

    public static bool DoesSaveGameExist(string name)
    {
        return File.Exists(GetSavePath(name));
    }
}
