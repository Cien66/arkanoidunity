﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class GlobalSaveObject 
{
    public LevelControllerSave gameManager = new LevelControllerSave();

    public List<CubeSave> cubes = new List<CubeSave>();

    public BallSave ball = new BallSave();
}


static public class SerializableGameplay
{
    public static LevelControllerSave SerializelevelController(LevelController levelController)
    {
        LevelControllerSave gameManagerSave = new LevelControllerSave();

        gameManagerSave.level = levelController.Level;
        gameManagerSave.score = levelController.Score;
        gameManagerSave.health = levelController.Health;

        gameManagerSave.levelSpeedBall = levelController.LevelSpeedBall;
        gameManagerSave.currentSpeedBall = levelController.CurrentSpeedBall;

        gameManagerSave.currentSpeedPalette = levelController.CurrentSpeedPalette;
        gameManagerSave.levelSpeedPalette = levelController.LevelSpeedPalette;

        gameManagerSave.isSlowMotion = levelController.SlowMotion;
        gameManagerSave.timeSlowMotion = levelController.TimeSlowMotion;

        gameManagerSave.isGameOver = levelController.GameOver;

        gameManagerSave.currentPlayer = levelController.PlayerName;

        return gameManagerSave;
    }

    public static CubeSave SerializeCube(BasicCubeS basicCube)
    {
        CubeSave cubeSave = new CubeSave();

        cubeSave.type = basicCube.GetCubeType();

        cubeSave.x = basicCube.transform.position.x;
        cubeSave.y = basicCube.transform.position.y;

        return cubeSave;
    }

    public static BallSave SerializeBall(Ball ball)
    {
        BallSave ballSave = new BallSave();

        ballSave.speed = ball.Speed;

        ballSave.pX = ball.transform.position.x;
        ballSave.pY = ball.transform.position.y;

        ballSave.dX = ball.Direction.x;
        ballSave.dY = ball.Direction.y;

        return ballSave;
    }
}
