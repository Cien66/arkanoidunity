﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    #region begin Private
    List<ResultSave> resultSave = new List<ResultSave>();

    string newPlayer;

    static GameController instance;

    GlobalSaveObject saveObject;

    bool isLoaded = false;
    #endregion

    #region begin Serialized
    [SerializeField] GameObject continueButton;

    [SerializeField] InputField inputField;

    [SerializeField] Text nameText;

    [SerializeField] Text scoreText;
    #endregion

    #region begin Property
    public List<ResultSave> TopTable { get { return resultSave; } }

    static public GameController Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType(typeof(GameController)) as GameController;

                if (instance == null)
                {
                    GameController prefab = Resources.Load("GameController", typeof(GameController)) as GameController;
                    instance = Instantiate(prefab) as GameController;

                    Debug.LogError("Created game controller" + instance.gameObject.name);
                }
            }

            return instance;
        }
    }

    public GlobalSaveObject SaveObject { get { return saveObject; } }

    public bool Loaded { get { return isLoaded; } }

    public string PlayerName { get { return newPlayer; } }
    #endregion

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        DontDestroyOnLoad(this.gameObject);

        instance = this;
    }

    private void Start()
    {
        if (!SaveGameSystem.DoesSaveGameExist("mySave.sav"))
        {
            continueButton.active = false;
        }
        else
        {
            saveObject = SaveGameSystem.LoadGame("mySave.sav");
            if (saveObject.gameManager.isGameOver)
                continueButton.active = false;
            else
                continueButton.active = true;
        }

        if (!SaveGameSystem.DoesSaveGameExist("board.sav"))
        {
            for (int i = 0; i < 10; ++i)
            {
                ResultSave result = new ResultSave();
                result.Score = 0;
                result.PlayerName = "NONE";


                resultSave.Add(result);
            }
        }
        else
        {
            resultSave = SaveGameSystem.LoadTopTable("board.sav");
        }
    }

    private void OnLevelWasLoaded(int level)
    {
        if (Application.loadedLevel == 3)
        {
            GameObject inputF = GameObject.FindGameObjectWithTag("InputUI");
            inputField = inputF.GetComponent<InputField>();
        }

        if (Application.loadedLevel == 2)
        {
            if (resultSave == null)
            {
                ClearTopTable();
            }

            PresentResults();
        }

        if (level == 0)
        {
            if (!continueButton)
                continueButton = GameObject.Find("ContinueGame");

            saveObject = SaveGameSystem.LoadGame("mySave.sav");

            if (continueButton)
            {
                if (!SaveGameSystem.DoesSaveGameExist("mySave.sav"))
                {
                    continueButton.active = false;
                }
                else
                {
                    if (saveObject != null)
                    {
                        if (saveObject.gameManager.isGameOver)
                            continueButton.active = false;
                        else
                            continueButton.active = true;
                    }
                }
            }
        }
    }

    public void ContinueGame()
    {
        Time.timeScale = 1.0f;


        if (Application.loadedLevel == 0)
        {
            saveObject = SaveGameSystem.LoadGame("mySave.sav");

            isLoaded = true;

            Cursor.visible = false;


            Application.LoadLevel(1);
        }
    }

    public void NewGame()
    {
        isLoaded = false;

        newPlayer = inputField.textComponent.text;

        Application.LoadLevel(1);
    }

    public void TableResults()
    {
        if (SaveGameSystem.DeleteSaveGame("board.sav"))
            resultSave = SaveGameSystem.LoadTopTable("boards.sav");

        Application.LoadLevel(2);
    }

    public void ClearTopTable()
    {
        if (resultSave == null)
            resultSave = new List<ResultSave>();

        resultSave.Clear();

        for (int i = 0; i < 10; ++i)
        {
            ResultSave result = new ResultSave();

            result.PlayerName = "NONE";
            result.Score = 0;

            resultSave.Add(result);
        }

        SaveTopTable();
        
        Debug.Log("Clear table!");

        PresentResults();
    }

    public void PresentResults()
    {
        if (!nameText)
        {
            var ob = GameObject.Find("NameText");

            if (!ob)
                Debug.LogError("Nie NameText gameobject");
            else
            {
                nameText = ob.GetComponent<Text>();

            }


            if (!nameText)
                Debug.LogError("nameText null!");
        }

        if (!scoreText)
        {
            scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
            if (!scoreText)
                Debug.LogError("ScoreText null!");
        }

        nameText.text = "Player\n";
        scoreText.text = "Score\n";

        if (resultSave == null)
            Debug.LogError("toptable null!");

        foreach (var item in resultSave)
        {
            nameText.text += item.PlayerName + "\n";
            scoreText.text += item.Score + "\n";
        }
    }

    public void SortTopTable()
    {
        int k = 0;
        for (int j = 0; j < resultSave.Count - 1; ++j)
        {
            k = 0;
            for (int i = 0; i < resultSave.Count - j - 1; ++i)
            {
                if (resultSave[i].Score < resultSave[i + 1].Score)
                {
                    var result = resultSave[i];
                    resultSave[i] = resultSave[i + 1];
                    resultSave[i + 1] = result;

                    ++k;
                }
            }
            if (k == 0)
                break;
        }
    }

    public void AddNewResutl(string name, int score)
    {
        ResultSave item = new ResultSave();
        item.PlayerName = name;
        item.Score = score;

        resultSave.Add(item);

        SortTopTable();

        resultSave.Remove(resultSave[10]);

        SaveTopTable();
    }

    public void SaveTopTable()
    {
        SaveGameSystem.SaveTopTable(resultSave, "boards.sav");
    }
}
