﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region begin Serialized
    [SerializeField] private float speed = 2.0f;

    [Space]
    [Header("Borders")]
    [Tooltip("Max value for left x palette position")]
    [SerializeField]
    private float maxLeft = -15.0f;

    [Tooltip("Max value for right x palette position")]
    [SerializeField]
    private float maxRight = 15.0f;
    #endregion Serialized

    #region begin Private
    private float direction = 0.0f;
    #endregion Private

    #region begin Property
    public float Speed
    {
        get { return speed; }
        set { speed = value; }
    }
    #endregion

    // Update is called once per frame
    void Update()
    {
        direction = Input.GetAxis("Horizontal");

        if (direction != 0.0f)
            Move();
    }

    void Move()
    {
        float x = transform.position.x + speed * Time.deltaTime * direction;

        x = Mathf.Clamp(x, maxLeft, maxRight);

        transform.position = new Vector3(x, transform.position.y, 0);
    }
}
